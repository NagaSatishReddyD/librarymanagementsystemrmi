package com.comp6231.rmi.interfac;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 
 * @author Naga Satish Reddy
 *
 */
public interface LibraryManagementInterface extends Remote{

	/**
	 * addItem method is used to add books into the library by manager.
	 * @param managerId
	 * @param itemId
	 * @param itemName
	 * @param quantity
	 * @return 
	 * @throws RemoteException
	 * @throws IOException 
	 * @throws SecurityException 
	 */
	public String addItem(String managerId,String itemId,String itemName,int quantity) throws RemoteException, SecurityException, IOException;
	
	/**
	 * removeItem is to remove books from the library by manager.
	 * @param nmanagerId
	 * @param itemId
	 * @param quantity
	 * @return 
	 * @throws RemoteException
	 */
	public String removeItem(String nmanagerId, String itemId,int quantity) throws RemoteException;
	
	/**
	 * listItemAvailability is to list the books available by manager.
	 * @param managerId
	 * @return 
	 * @throws RemoteException
	 * @throws IOException 
	 * @throws SecurityException 
	 */
	public String listItemAvailability(String managerId) throws RemoteException, SecurityException, IOException;
	
	
	/**
	 * borrowItem is used by user to borrow book
	 * @param userId
	 * @param itemId
	 * @return 
	 * @throws RemoteException
	 * @throws IOException 
	 * @throws SecurityException 
	 */
	public String borrowItem(String userId, String itemId) throws RemoteException, SecurityException, IOException;
	
	
	/**
	 * findItem is used to find the item by the user.
	 * @param userId
	 * @param itemId
	 * @return 
	 * @throws RemoteException
	 * @throws IOException 
	 * @throws SecurityException 
	 */
	public String findItem(String userId, String itemId, boolean fromOtherServer) throws RemoteException, SecurityException, IOException;
	
	/**
	 * returnItem is used to retun the book by user.
	 * @param userId
	 * @param itemId
	 * @return 
	 * @throws RemoteException
	 * @throws IOException 
	 * @throws SecurityException 
	 */
	public String returnItem(String userId, String itemId) throws RemoteException, SecurityException, IOException;

	/**
	 * addToWaitingList method is used to add the user to the waiting list
	 * @param userId
	 * @param itemId
	 * @return
	 * @throws IOException 
	 * @throws SecurityException 
	 */
	public String addToWaitingList(String userId, String itemId) throws RemoteException, SecurityException, IOException;
}
