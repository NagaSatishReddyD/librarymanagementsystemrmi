package com.comp6231.rmi.client;

import java.io.IOException;
import java.rmi.NotBoundException;

/**
 * UserClient is the starting of the client interface
 * @author Naga Satish Reddy
 *
 */
public class UserClient {

	public static void main(String[] args) throws NotBoundException, IOException {
		UserMethodsImplementaion validation = new UserMethodsImplementaion();
		System.out.println("Enter user Id :");
		String userId = validation.getUser();
		validation.showOperations(userId);
	}
}
