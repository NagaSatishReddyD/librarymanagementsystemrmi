package com.comp6231.rmi.server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.rmi.AlreadyBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import com.comp6231.rmi.constants.LibraryManagementConstants;
import com.comp6231.rmi.impl.McgillLibraryImpl;

/**
 * McgillLibrarayServer class is the starting the mcgill library server
 * @author Naga Satish Reddy
 *
 */
public class McgillLibrarayServer {

	public static void main(String[] args) throws IOException, AlreadyBoundException {
		
		McgillLibraryImpl mcgillLibraryImpl = new McgillLibraryImpl();
		Registry registry = LocateRegistry.createRegistry(LibraryManagementConstants.MCGILL_SERVER_PORT);
		
		Runnable libraryServerRunnable = () ->{
			try {
				handlesRequestFromAnotherServers(mcgillLibraryImpl, registry);
			} catch (IOException e) {
				System.out.println("Error occured : "+ e.getMessage());
			}
		};
		
		Runnable mcgillRequestServerRunnable = () ->{
			try {
				handleMcgillRequests(mcgillLibraryImpl, registry);
			} catch (IOException | AlreadyBoundException e) {
				System.out.println("Couldn't connect the server..."+e.getMessage());
			}
		};
		
		new Thread(libraryServerRunnable).start();
		new Thread(mcgillRequestServerRunnable).start();
	}

	/**
	 * handleMcgillRequests method to handle the requests of the mcgill users
	 * @param registry 
	 * @param mcgillLibraryImpl 
	 * @throws IOException
	 * @throws AlreadyBoundException
	 */
	private static void handleMcgillRequests(McgillLibraryImpl mcgillLibraryImpl, Registry registry) throws IOException, AlreadyBoundException {
		registry.bind("MCGILL_SERVER", mcgillLibraryImpl);
	}

	/**
	 * handlesRequestFromAnotherServers method to handle requests from the other servers
	 * @param registry 
	 * @param mcgillLibraryImpl 
	 * @throws IOException
	 */
	private static void handlesRequestFromAnotherServers(McgillLibraryImpl mcgillLibraryImpl, Registry registry) throws IOException {
		DatagramSocket socket = null;
		try {
			socket = new DatagramSocket(LibraryManagementConstants.MCGILL_SERVER_PORT);
			System.out.println("McGill Server started...");
			byte [] message = new byte[1000];
			while(true) {
				DatagramPacket recievedDatagramPacket = new DatagramPacket(message, message.length);
				socket.receive(recievedDatagramPacket);
				String response = mcgillLibraryImpl.handleRequestFromOtherServer(new String(recievedDatagramPacket.getData()));
				DatagramPacket reply = new DatagramPacket(response.getBytes(), response.length(), recievedDatagramPacket.getAddress(),
						recievedDatagramPacket.getPort());
				socket.send(reply);
			}
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(socket != null)
				socket.close();
		}
	}

}
