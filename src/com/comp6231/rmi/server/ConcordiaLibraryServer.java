package com.comp6231.rmi.server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import com.comp6231.rmi.constants.LibraryManagementConstants;
import com.comp6231.rmi.impl.ConcordiaLibraryImpl;

public class ConcordiaLibraryServer {

	public static void main(String[] args) throws AlreadyBoundException, IOException {
		ConcordiaLibraryImpl concordiaLibraryImpl = new ConcordiaLibraryImpl();
		Registry registry = LocateRegistry.createRegistry(LibraryManagementConstants.CONCORDIA_SERVER_PORT);
		
		Runnable libraryServerRunnable = () ->{
			try {
				handlesRequestFromAnotherServers(concordiaLibraryImpl, registry);
			} catch (IOException e) {
				System.out.println("Error occured : "+ e.getMessage());
			}
		};
		
		Runnable concordiaRequestServerRunnable = () ->{
			try {
				handleConcordiaRequests(concordiaLibraryImpl, registry);
			} catch (IOException | AlreadyBoundException e) {
				System.out.println("Couldn't connect the server..."+e.getMessage());
			}
		};
		
		new Thread(libraryServerRunnable).start();
		new Thread(concordiaRequestServerRunnable).start();
	}

	private static void handleConcordiaRequests(ConcordiaLibraryImpl concordiaLibraryImpl, Registry registry) throws AccessException, RemoteException, AlreadyBoundException {
		registry.bind("CONCORDIA_SERVER", concordiaLibraryImpl);
	}

	private static void handlesRequestFromAnotherServers(ConcordiaLibraryImpl concordiaLibraryImpl, Registry registry) throws IOException {
		DatagramSocket socket = null;
		try {
			socket = new DatagramSocket(LibraryManagementConstants.CONCORDIA_SERVER_PORT);
			System.out.println("Concordia Server started...");
			byte [] message = new byte[1000];
			while(true) {
				DatagramPacket recievedDatagramPacket = new DatagramPacket(message, message.length);
				socket.receive(recievedDatagramPacket);
				System.out.println(recievedDatagramPacket.getData());
				String response = concordiaLibraryImpl.handleRequestFromOtherServer(new String(recievedDatagramPacket.getData()));
				DatagramPacket reply = new DatagramPacket(response.getBytes(), recievedDatagramPacket.getLength(), recievedDatagramPacket.getAddress(),
						recievedDatagramPacket.getPort());
				socket.send(reply);
			}
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(socket != null)
				socket.close();
		}
	}
}
