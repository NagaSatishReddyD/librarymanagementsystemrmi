package com.comp6231.rmi.server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import com.comp6231.rmi.constants.LibraryManagementConstants;
import com.comp6231.rmi.impl.MontrealLibraryImpl;

public class MontrealLibraryServer {


	public static void main(String[] args) throws AlreadyBoundException, IOException {
		MontrealLibraryImpl montrealLibraryImpl = new MontrealLibraryImpl();
		Registry registry = LocateRegistry.createRegistry(LibraryManagementConstants.MONTREAL_SERVER_PORT);

		Runnable libraryServerRunnable = () ->{
			try {
				handlesRequestFromAnotherServers(montrealLibraryImpl, registry);
			} catch (IOException e) {
				System.out.println("Error occured : "+ e.getMessage());
			}
		};

		Runnable mcgillRequestServerRunnable = () ->{
			try {
				handleMcgillRequests(montrealLibraryImpl, registry);
			} catch (IOException | AlreadyBoundException e) {
				System.out.println("Couldn't connect the server..."+e.getMessage());
			}
		};

		new Thread(libraryServerRunnable).start();
		new Thread(mcgillRequestServerRunnable).start();
	}

	private static void handleMcgillRequests(MontrealLibraryImpl montrealLibraryImpl, Registry registry) throws AccessException, RemoteException, AlreadyBoundException {
		registry.bind("MONTREAL_SERVER", montrealLibraryImpl);
	}

	private static void handlesRequestFromAnotherServers(MontrealLibraryImpl montrealLibraryImpl, Registry registry) throws IOException {
		DatagramSocket socket = null;
		try {
			socket = new DatagramSocket(LibraryManagementConstants.MONTREAL_SERVER_PORT);
			System.out.println("Montreal Server started...");
			byte [] message = new byte[1000];
			while(true) {
				DatagramPacket recievedDatagramPacket = new DatagramPacket(message, message.length);
				socket.receive(recievedDatagramPacket);
				String response = montrealLibraryImpl.handleRequestFromOtherServer(new String(recievedDatagramPacket.getData()));
				DatagramPacket reply = new DatagramPacket(response.getBytes(), response.length(), recievedDatagramPacket.getAddress(),
						recievedDatagramPacket.getPort());
				socket.send(reply);
			}
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(socket != null)
				socket.close();
		}
	}

}
