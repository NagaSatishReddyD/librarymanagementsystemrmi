package com.comp6231.rmi.impl;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Map;

import com.comp6231.rmi.constants.LibraryManagementConstants;
import com.comp6231.rmi.interfac.LibraryManagementInterface;
import com.comp6231.rmi.server.BookData;

/**
 * ConcordiaLibraryImpl is used to implement the methods used by the concordia users.
 * @author Naga Satish Reddy Dwarampudi
 *
 */
public class ConcordiaLibraryImpl extends UnicastRemoteObject implements LibraryManagementInterface {

	private static Map<String, BookData> concordiaBooksData;
	LibraryImplementationHelper implementationHelper = new LibraryImplementationHelper();

	public ConcordiaLibraryImpl() throws IOException {
		super();
		concordiaBooksData = implementationHelper.loadLibraryData(System.getProperty("user.dir")+LibraryManagementConstants.CONCORDIA_INITAL_LOAD_FILE, 
				System.getProperty("user.dir")+LibraryManagementConstants.CONCORDIA_SERVER_LOG_FILE);
	}

	@Override
	public String addItem(String managerId, String itemId, String itemName, int quantity) throws SecurityException, IOException {
		implementationHelper.insertLog(managerId+" : Added "+itemId+" : "+itemName+" : "+ " : "+quantity);
		String response =  implementationHelper.addBookToLibrary(concordiaBooksData, itemId, itemName, quantity);
		implementationHelper.insertLog(managerId+" : Server Response Added "+response);
		return response;
	}

	@Override
	public String removeItem(String managerId, String itemId, int quantity) throws RemoteException {
		implementationHelper.insertLog(managerId+" : "+"Removed"+" : "+ itemId + " : "+ quantity);
		String response =  implementationHelper.removeItem(concordiaBooksData, itemId, quantity);
		implementationHelper.insertLog(managerId+" : "+"Server Response Removed"+" : "+ itemId + " : "+ quantity);
		return response;
	}

	@Override
	public String listItemAvailability(String managerId) throws SecurityException, IOException {
		implementationHelper.insertLog(managerId+" : Listed Items");
		String response = implementationHelper.listItemAvailable(concordiaBooksData);
		implementationHelper.insertLog(managerId+" : Listed Items :"+ response);
		return response;
	}

	@Override
	public String borrowItem(String userId, String itemId) throws SecurityException, IOException {
		String response;
		implementationHelper.insertLog(userId+" : Borrow "+" : "+itemId);
		boolean isFromOtherServers = userId.subSequence(0, 3).equals(LibraryManagementConstants.CONCORDIA_CODE) ? false : true;
		if(itemId.substring(0, 3).equals(LibraryManagementConstants.CONCORDIA_CODE))
			response = implementationHelper.borrowItem(concordiaBooksData, userId, itemId, isFromOtherServers);
		else
			response = implementationHelper.requestOtherLibraryServers(userId, itemId, 1, null);
		implementationHelper.insertLog(userId+" : Borrow Response"+" : "+itemId+" : "+response);
		return response;
	}
	
	@Override
	public String addToWaitingList(String userId, String itemId) throws SecurityException, IOException{
		String response;
		implementationHelper.insertLog(userId+": Added to waiting list for : "+itemId);
		if(itemId.substring(0, 3).equals(LibraryManagementConstants.CONCORDIA_CODE))
			response = implementationHelper.addToWaitingList(concordiaBooksData, userId, itemId);
		else
			response = implementationHelper.requestOtherLibraryServers(userId, itemId, 4, null);
		implementationHelper.insertLog(userId+": Added to waiting list for : "+itemId+" "+response);
		return response;
	}
	

	@Override
	public String findItem(String userId, String itemName, boolean fromOtherServer) throws SecurityException, IOException {
		String response = "";
		implementationHelper.insertLog(userId+" : Find : "+itemName);
		response += implementationHelper.findItem(concordiaBooksData, userId, itemName);
		if(!fromOtherServer) {
			response += implementationHelper.requestOtherLibraryServers(userId,itemName,3,LibraryManagementConstants.MONTREAL_CODE);
			response += implementationHelper.requestOtherLibraryServers(userId,itemName,3,LibraryManagementConstants.MCGILL_CODE);
			response = response.substring(0, response.length() - 1);
		}
		implementationHelper.insertLog(userId+" : Find Response: "+itemName +" "+response);
		return response;
	}

	@Override
	public String returnItem(String userId, String itemId) throws SecurityException, IOException {
		String response;
		implementationHelper.insertLog(userId+" : Returned "+" : "+itemId);
		if(itemId.substring(0, 3).equals(LibraryManagementConstants.CONCORDIA_CODE))
			response =  implementationHelper.returnItem(concordiaBooksData, userId, itemId);
		else
			response =  implementationHelper.requestOtherLibraryServers(userId, itemId, 2, null);
		implementationHelper.insertLog(userId+" : Returned Response"+" : "+response);
		return response;
	}

	/**
	 * handleRequestFromOtherServer methods calls the respective methods by parsing the data from other servers
	 * @param dataFromAnotherServer
	 * @return
	 * @throws SecurityException
	 * @throws IOException
	 */
	public String handleRequestFromOtherServer(String dataFromAnotherServer) throws SecurityException, IOException {
		String[] dataString = dataFromAnotherServer.split(",");
		switch(dataString[0]) {
		case "1": return borrowItem(dataString[2], dataString[1]);
		case "2": return returnItem(dataString[2], dataString[1]);
		case "3": return findItem(dataString[2], dataString[1], true);
		}
		return "Unknown request";
	}

}
