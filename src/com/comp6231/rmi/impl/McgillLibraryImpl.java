package com.comp6231.rmi.impl;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Map;

import com.comp6231.rmi.constants.LibraryManagementConstants;
import com.comp6231.rmi.interfac.LibraryManagementInterface;
import com.comp6231.rmi.server.BookData;

public class McgillLibraryImpl extends UnicastRemoteObject implements LibraryManagementInterface{

	private static Map<String, BookData> mcgillBooksData;
	LibraryImplementationHelper implementationHelper = new LibraryImplementationHelper();

	public McgillLibraryImpl() throws IOException {
		super();
		mcgillBooksData = implementationHelper.loadLibraryData(System.getProperty("user.dir")+LibraryManagementConstants.MCGGILL_INITAL_LOAD_FILE,
				System.getProperty("user.dir")+LibraryManagementConstants.MCGILL_SERVER_LOG_FILE);
	}

	@Override
	public String addItem(String managerId, String itemId, String itemName, int quantity) throws SecurityException, IOException {
		implementationHelper.insertLog(managerId+" : Added "+itemId+" : "+itemName+" : "+ " : "+quantity);
		String response =  implementationHelper.addBookToLibrary(mcgillBooksData, itemId, itemName, quantity);
		implementationHelper.insertLog(managerId+" : Server Response Added "+response);
		return response;
	}

	@Override
	public String removeItem(String managerId, String itemId, int quantity) throws RemoteException {
		implementationHelper.insertLog(managerId+" : "+"Removed"+" : "+ itemId + " : "+ quantity);
		String response =  implementationHelper.removeItem(mcgillBooksData, itemId, quantity);
		implementationHelper.insertLog(managerId+" : "+"Server Response Removed"+" : "+ itemId + " : "+ quantity);
		return response;
	}

	@Override
	public String listItemAvailability(String managerId) throws SecurityException, IOException {
		implementationHelper.insertLog(managerId+" : Listed Items");
		String response = implementationHelper.listItemAvailable(mcgillBooksData);
		implementationHelper.insertLog(managerId+" : Listed Items :"+ response);
		return response;
	}

	@Override
	public String borrowItem(String userId, String itemId) throws RemoteException {
		String response;
		implementationHelper.insertLog(userId+" Borrow : "+itemId);
		boolean isFromOtherServers = userId.subSequence(0, 3).equals(LibraryManagementConstants.MCGILL_CODE) ? false : true;
		if(itemId.substring(0, 3).equals(LibraryManagementConstants.MCGILL_CODE))
			response = implementationHelper.borrowItem(mcgillBooksData, userId, itemId, isFromOtherServers);
		else
			response = implementationHelper.requestOtherLibraryServers(userId, itemId, 1, null);
		implementationHelper.insertLog(userId+" : Borrow Response"+" : "+itemId+" : "+response);
		return response;
	}

	@Override
	public String addToWaitingList(String userId, String itemId) throws RemoteException{
		implementationHelper.insertLog(userId+": Added to waiting list for : "+itemId);
		if(itemId.substring(0, 3).equals(LibraryManagementConstants.CONCORDIA_CODE))
			return implementationHelper.addToWaitingList(mcgillBooksData, userId, itemId);
		else
			return implementationHelper.requestOtherLibraryServers(userId, itemId, 4, null);
	}

	@Override
	public String findItem(String userId, String itemName,boolean fromOtherServers) throws RemoteException {
		String response = "";
		implementationHelper.insertLog(userId+" : Find : "+itemName);
		response += implementationHelper.findItem(mcgillBooksData, userId, itemName);
		if(!fromOtherServers) {
			response += implementationHelper.requestOtherLibraryServers(userId,itemName,3,LibraryManagementConstants.CONCORDIA_CODE);
			response += implementationHelper.requestOtherLibraryServers(userId,itemName,3,LibraryManagementConstants.MONTREAL_CODE);
			response = response.substring(0, response.length() - 1);
		}
		implementationHelper.insertLog(userId+"Find Response : "+ response);
		return response;
	}

	@Override
	public String returnItem(String userId, String itemId) throws SecurityException, IOException {
		String response;
		implementationHelper.insertLog(userId+" : Returned "+" : "+itemId);
		if(itemId.substring(0, 3).equals(LibraryManagementConstants.MCGILL_CODE))
			response = implementationHelper.returnItem(mcgillBooksData, userId, itemId);
		else
			response = implementationHelper.requestOtherLibraryServers(userId, itemId, 2, null);
		implementationHelper.insertLog(userId+" : Returned Response"+" : "+response);
		return response;
	}

	/**
	 * handleRequestFromOtherServer methods calls the respective methods by parsing the data from other servers
	 * @param dataFromAnotherServer
	 * @return
	 * @throws IOException 
	 * @throws SecurityException 
	 */
	public String handleRequestFromOtherServer(String dataFromAnotherServer) throws SecurityException, IOException {
		String[] dataString = dataFromAnotherServer.split(",");
		switch(dataString[0]) {
		case "1": return borrowItem(dataString[2].trim(), dataString[1]);
		case "2": return returnItem(dataString[2].trim(), dataString[1]);
		case "3": return findItem(dataString[2].trim(), dataString[1], true);
		}
		return "Unknown request";
	}

}
